from flask import Flask

app = Flask(__name__)


@app.route('/', methods=["GET", "POST", "OPTIONS"])
def helloWorld():
    return "<h1>Hello World! This is Palash from Quantiphi</h1>"

if __name__ == "__main__":
    app.run(host='0.0.0.0', port='5000')